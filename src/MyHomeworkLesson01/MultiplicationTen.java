/*
Write a program that prints the product of the first ten positive integers,
1 × 2 × ... × 10. (Use * to indicate multiplication in Java.)
*/

package MyHomeworkLesson01;

public class MultiplicationTen {
    public static void main(String[] args) {
        System.out.println("1x2x3x4x5x6x7x8x9x10");
        System.out.println("равно");
        short summary = (short) (1*2*3*4*5*6*7*8*9*10);
        System.out.println(summary);
    }
}
