/*
Write a program that prints the balance of an account after the first, second, and
third year. The account has an initial balance of $1,000 and earns 5 percent interest
per year.
*/

package MyHomeworkLesson01;

public class BalanceAfterThirdYear {
    public static void main(String[] args)
        {
            int account = 1000;
            System.out.println("first year - " + account);
            double account2 = account*1.05;
            System.out.println("second year - " + account2);
            double account3 = account2*1.05;
            System.out.println("third year - " + account3);
        }
}
