/*Write a program that initializes an array with ten random integers and then prints
four lines of output, containing
•    Every element at an even index.
•    Every even element.
•    All elements in reverse order.
•    Only the first and last element.

import java.util.Scanner;

public class P6_01 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] array = new int[10];
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%d element of array: ", i);
            array[i] = input.nextInt();
        }
        input.close();

        evenIndexElements(array);
        everyEvenElement(array);
        allElementsInReverseOrder(array);
        firstAndLastElement(array);
    }

    public static void evenIndexElements(int[] array) {
        ...
    }

    public static void everyEvenElement(int[] array) {
        ...
    }

    public static void allElementsInReverseOrder(int[] array) {
        ...
    }

    public static void firstAndLastElement(int[] array) {
        ...
    }
}
*/
