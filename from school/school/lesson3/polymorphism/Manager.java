package school.lesson3.polymorphism;

public class Manager extends Employee {
   
   
   
   public Manager(String name, double salary, int reports) {
      super(name, salary);
      this.reports = reports;
   }

   public void setReports(int reports) {
      this.reports = reports;
   }

   public int getReports() {
      return reports;
   }

   public String getDescription(){
      return "A manager: name = " + getName() + ", salary = "
            + getSalary() + ", direct reports = " + getReports();
   }
  
   private int reports;
}
