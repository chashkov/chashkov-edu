package school.lesson3.polymorphism;

class DescriptionPrinter {
   
   public static void printDescriptions(Person[] people) {

      for (Person person : people) {
         System.out.println(person.getDescription());
      }
   }
}