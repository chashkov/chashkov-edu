package school.lesson3.inheritance;

class Person {

   public Person() {
      
   }
   
   public Person(String name){
      this.name = name;
      System.out.println("Person constructor is called setting name = " + name);
   }
   
   public void setName(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   
   public String getDescription() {
      return "A person: name = " + getName();
   }
   
   /*
   public String getDescription() {
      return "[name = " + getName()+ "]";
   }
   */
   
   private String name;
}