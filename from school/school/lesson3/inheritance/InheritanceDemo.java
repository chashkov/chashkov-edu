package school.lesson3.inheritance;

public class InheritanceDemo {

   public static void main(String[] args) {

      Person person = new Person();
      person.setName("Robert");
      System.out.println("A person: name = " + person.getName());

      Employee employee = new Employee();
      employee.setName("Robert");
      employee.setSalary(30000);
      System.out.println("An employee: name = " + employee.getName()
            + ", salary = " + employee.getSalary());

      Manager manager = new Manager();
      manager.setName("Robert");
      manager.setSalary(30000);
      manager.setReports(5);
      System.out.println("A manager: name = " + manager.getName()
            + ", salary = " + manager.getSalary() + ", reports = "
            + manager.getReports());
   }
}
