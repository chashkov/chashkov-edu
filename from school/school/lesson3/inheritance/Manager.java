package school.lesson3.inheritance;

public class Manager extends Employee {

   public Manager() {
   }   
   
   public Manager(String name, double salary, int reports) {
      super(name, salary);
      this.reports = reports;
      System.out.println("Manager constructor is called setting reports = " + reports);
   }

   public void setReports(int reports) {
      this.reports = reports;
   }

   public int getReports() {
      return reports;
   }

   
   public String getDescription(){
      return "A manager: name = " + getName() + ", salary = "
            + getSalary() + ", direct reports = " + getReports();
   }
   
   
   /*
   public String getDescription() {
      return super.getDescription() + "[direct reports = " + getReports()+ "]";
   }
   */
   
   private int reports;
}
