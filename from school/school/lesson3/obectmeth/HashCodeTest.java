package school.lesson3.obectmeth;

import java.util.HashSet;
import java.util.Set;

public class HashCodeTest {
 
   public static void main(String[] args) {
      
       System.out.println("Creating HashSet to store Employee objects ...");
       Set<Employee> set = new HashSet<Employee>();
 
       System.out.println("Creating two equal objects ...");
       Employee one = new Employee("Alice Coder", 25000, 1987, 12, 15);
       Employee two = new Employee("Alice Coder", 25000, 1987, 12, 15);
       
       System.out.println("Just to be absolutely sure let's test them for equality!");
       System.out.println("Objects are equal: " + one.equals(two));
       
       System.out.println("Adding one of them to HashSet ...");
       set.add(one);
       
       System.out.println("Hash set contains another one: " + set.contains(two));

       System.out.println("Should be true if hashCode is overriden correctly.");
       System.out.println("May be false if hashCode is not overriden!");
   }
}
