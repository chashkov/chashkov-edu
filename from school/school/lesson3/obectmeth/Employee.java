package school.lesson3.obectmeth;

import java.util.Date;
import java.util.GregorianCalendar;

class Employee implements Cloneable {
   public Employee(String n, double s, int year, int month, int day) {
      name = n;
      salary = s;
      GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
      hireDay = calendar.getTime();
   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

   public void setHireDay(int year, int month, int day) {
      Date newHireDay = new GregorianCalendar(year, month - 1, day).getTime();
      hireDay.setTime(newHireDay.getTime());
   }
   
   public Date getHireDay() {
      return hireDay;
   }

   public void raiseSalary(double byPercent) {
      double raise = salary * byPercent / 100;
      salary += raise;
   }

   public boolean equals(Object otherObject) {

      if (this == otherObject)
         return true;

      if (otherObject == null)
         return false;

      if (getClass() != otherObject.getClass())
         return false;

      Employee other = (Employee) otherObject;

      if (name == null) {
         if (other.name != null)
            return false;
      } else if (!name.equals(other.name))
         return false;
      
      
      return  salary == other.salary && hireDay.equals(other.hireDay);
   }

   public int hashCode() {
       return 7 * name.hashCode() + 11 * new Double(salary).hashCode() + 13
           * hireDay.hashCode();
   }

   public String toString() {
      return getClass().getName() + "[name=" + name + ",salary=" + salary
            + ",hireDay=" + hireDay + "]";
   }

   public Employee clone() throws CloneNotSupportedException {
      Employee cloned = (Employee) super.clone();
      cloned.hireDay = (Date) hireDay.clone();
           
      return cloned;
   }
   
   private String name;
   private double salary;
   private Date hireDay;
}