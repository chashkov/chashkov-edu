package school.lesson3.obectmeth;

public class AnotherEqualsDemo {
   public static void main(String[] args) {

      System.out.println("Creating Employee object for Alice Coder ...");
      Employee alice = new Employee("Alice Coder", 25000, 1987, 12, 15);
      System.out.println("alice: " + alice + "\n");
            
      System.out.println("Creating Manager object for Alice Coder ...");
      Employee alicemanager = new Manager("Alice Coder", 25000, 1987, 12, 15);
      System.out.println("alicemanager: " + alicemanager + "\n");
             
      System.out.println("alice and alicemanager reference equal objects: " + alice.equals(alicemanager));
   }
}
