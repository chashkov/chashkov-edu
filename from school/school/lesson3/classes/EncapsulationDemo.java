package school.lesson3.classes;

import java.util.*;

public class EncapsulationDemo {
   public static void main(String[] args) {

      EncapsulationEmployee harry = new EncapsulationEmployee("Harry Hacker", 50000, 1989, 10, 1);

      System.out.println("name=" + harry.getName() + ",salary="
            + harry.getSalary() + ",hireDay=" + harry.getHireDay());

      Date d = harry.getHireDay();

      double tenYearsInMilliSeconds = 10 * 365.25 * 24 * 60 * 60 * 1000;
      d.setTime(d.getTime() - (long) tenYearsInMilliSeconds);

      System.out.println("name=" + harry.getName() + ",salary="
            + harry.getSalary() + ",hireDay=" + harry.getHireDay());
   }
}

class EncapsulationEmployee {
   public EncapsulationEmployee(String n, double s, int year, int month, int day) {
      name = n;
      salary = s;
      GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
      hireDay = calendar.getTime();
   }

   public String getName() {
      return name;
   }

   public double getSalary() {
      return salary;
   }

   public Date getHireDay() {
      return (Date) hireDay.clone();
      //return hireDay;
   }

   private double salary;
   private String name;
   private Date hireDay;
}
