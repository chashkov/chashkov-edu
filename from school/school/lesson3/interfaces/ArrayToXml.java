package school.lesson3.interfaces;

public class ArrayToXml {

   static String getXml(ToXmlable[] toXML) {

      StringBuilder sb = new StringBuilder("<array>\n");
      
      for(ToXmlable t: toXML) {
         sb.append(t.toXmlString());         
      }
      
      sb.append("</array>\n");
      return sb.toString();
   }
}
