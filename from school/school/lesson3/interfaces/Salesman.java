package school.lesson3.interfaces;

public class Salesman extends Employee implements Comparable{
   
   public Salesman(String name, double salary, double bonus) {
      super(name, salary);
      this.bonus = bonus;
   }

   public double getSalary() {
      return super.getSalary() + bonus;
   }

   public String toString() {
      return "Salesman [name=" + getName() + ", salary=" + getSalary() + ", bonus=" + bonus + "]";
   }
   
   public int compareTo(Comparable other) {
      
      double otherBonus = ((Salesman)other).bonus;
      return (bonus > otherBonus ? 1 :(bonus < otherBonus ? -1: 0));
   }
   private double bonus;
}
