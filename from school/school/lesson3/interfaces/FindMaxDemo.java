package school.lesson3.interfaces;

import java.util.Arrays;

public class FindMaxDemo {

   public static void main(String[] args) {

      Comparable[] comparables = new Comparable[5];
      
      /*
      comparables[0] = new Person("Tommy Tester", 1989);
      comparables[1] = new Person("Harry Hacker",1986);
      comparables[2] = new Person("Alice Coder", 1987);
      comparables[3] = new Person("Bobby Seller", 1972);
      comparables[4] = new Person("Ed Engineer", 1975);
      */
      
      comparables[0] = new Item("Toaster", 1234);
      comparables[1] = new Item("Kettle", 4562);
      comparables[2] = new Item("Microwave oven", 9912);
      comparables[3] = new Item("Coffemaker", 2912);
      comparables[4] = new Item("Blender", 1231);
      
      System.out.println("Array contents: ");
      for (Comparable comp : comparables)
         System.out.println(comp);
            
      System.out.println("\nLooking for the maximum ...");
      System.out.println("Maximum is: " + FindMax.max(comparables));
   }
}
