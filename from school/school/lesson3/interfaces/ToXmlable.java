package school.lesson3.interfaces;

public interface ToXmlable {

   String toXmlString();
}
