package school.lesson3.interfaces;

public class FindMax {

   public static Comparable max(Comparable[] toFindMax) {

      Comparable tempMax;
      if (toFindMax.length == 0)
         return null;

      tempMax = toFindMax[0];

      for (int i = 1; i < toFindMax.length; i++) {

         if (toFindMax[i].compareTo(tempMax) > 0) {
            tempMax = toFindMax[i];
         }
      }
      return tempMax;
   }
}
