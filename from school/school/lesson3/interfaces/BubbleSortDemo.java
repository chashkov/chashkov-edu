package school.lesson3.interfaces;

public class BubbleSortDemo {

   public static void main(String[] args) {

      Comparable[] comps = new Comparable[5];
            
      /*
      comps[0] = new Item("Toaster", 1234);
      comps[1] = new Item("Kettle", 4562);
      comps[2] = new Item("Microwave oven", 9912);
      comps[3] = new Item("Coffemaker", 2912);
      comps[4] = new Item("Blender", 1231);
      */

      /*
      comps[0] = new Person("Tommy Tester", 1989);
      comps[1] = new Person("Harry Hacker",1986);
      comps[2] = new Person("Alice Coder", 1987);
      comps[3] = new Person("Bobby Seller", 1972);
      comps[4] = new Person("Ed Engineer", 1975);
      */
      
      /*
      comps[0] = new Product("Shoes", 49.99);
      comps[1] = new Product("Raincoat", 59.99);
      comps[2] = new Product("Shirt", 14.99);
      comps[3] = new Product("Jacket", 74.99);
      comps[4] = new Product("Coat", 99.99);
      */
      
      comps[0] = new Salesman("Abby Normal", 28000, 8000);
      comps[1] = new Salesman("Steve Jackson", 19000, 2500);
      comps[2] = new Salesman("Ed Sell", 25000, 4000);
      comps[3] = new Salesman("John Whatson", 24000, 3000);
      comps[4] = new Salesman("Bobby Seller", 30000, 2000);
      
      System.out.println("Array contents: ");
      for (Comparable comp : comps)
         System.out.println(comp);

      System.out.println("\nSorting ...\n");

      BubbleSort.sort(comps);

      System.out.println("Array contents: ");
      for (Comparable comp : comps)
         System.out.println(comp);
   }
}

/*
compArr[0] = new Book("Martin Fowler", "Refactoring");
compArr[1] = new Book("Herbert Schildt", "Java The Complete Reference");
compArr[2] = new Book("Joshua Bloch", "Effective Java");
compArr[3] = new Book("Bruce Eckel","Thinking in Java");
compArr[4] = new Book("Cay S. Horstmann", "Core Java");
*/

/*
compArr[0] = new Appliance("Toaster", 1234);
compArr[1] = new Appliance("Kettle", 4562);
compArr[2] = new Appliance("Microwave oven", 9912);
compArr[3] = new Appliance("Coffemaker", 2912);
compArr[4] = new Appliance("Blender", 1231);
*/
