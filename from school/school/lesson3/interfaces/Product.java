package school.lesson3.interfaces;

public class Product implements Comparable, ToXmlable {

   public Product(String name, double price) {
      this.name = name;
      this.price = price;
   }

   public String toString() {
      return "[name=" + name + ", price=" + price + "]";
   }

   public String toXmlString() {
      return "  <item>\n    <name> " + name + " </name> \n    <price> "
            + price + " </price>\n  </item>\n";
   }

   public int compareTo(Comparable other) {

      Product otherItem = (Product) other;
      return (price > otherItem.price ? 1 : (price < otherItem.price ? -1 : 0));
   }

   private String name;
   private double price;
}
