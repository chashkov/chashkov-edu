package school.lesson3.interfaces;

import java.io.FileWriter;
import java.io.IOException;

public class ArrayToXmlDemo {

   public static void main(String[] args) {

      ToXmlable[] toXmlables = new Product[5];

      toXmlables[0] = new Product("Shoes", 49.99);
      toXmlables[1] = new Product("Raincoat", 59.99);
      toXmlables[2] = new Product("Shirt", 14.99);
      toXmlables[3] = new Product("Jacket", 74.99);
      toXmlables[4] = new Product("Coat", 99.99);

      System.out.println("Array contents: ");
      for (ToXmlable t : toXmlables)
         System.out.println(t);
     
      System.out.println();
      System.out.println("Array as XML: ");
      String xml = ArrayToXml.getXml(toXmlables);
      System.out.println(xml);
      
      /*
      FileWriter out = null;

      try {
         out = new FileWriter("I:\\items.xml");
         out.write(xml);
      } catch (IOException e) {
         System.out.println("An I/O error occured");
      } finally {
         try {
            if (out != null)
               out.close();
         } catch (IOException e) {
            System.out.println("Error closing file");
         }
      }
      */
   }
}
