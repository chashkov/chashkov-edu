package school.lesson4.mutable;

public class IndexOfDemo {

   public static void main(String args[]) {

      String s = "Now is the time for all good men "
            + "to come to the aid of their country.";

      StringBuffer sb = new StringBuffer(s);

      System.out.println(sb);
      System.out.println("indexOf(t) = " + sb.indexOf("t"));
      System.out.println("lastIndexOf(t) = " + sb.lastIndexOf("t"));
      System.out.println("indexOf(the) = " + sb.indexOf("the"));
      System.out.println("lastIndexOf(the) = " + sb.lastIndexOf("the"));
      System.out.println("indexOf(t, 10) = " + sb.indexOf("t", 10));
      System.out.println("lastIndexOf(t, 60) = " + sb.lastIndexOf("t", 60));
      System.out.println("indexOf(the, 10) = " + sb.indexOf("the", 10));
      System.out.println("lastIndexOf(the, 60) = " + sb.lastIndexOf("the", 60));
   }
}