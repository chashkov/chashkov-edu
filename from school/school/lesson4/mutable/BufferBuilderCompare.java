package school.lesson4.mutable;

public class BufferBuilderCompare {
  
   public static void main(String[] args) {
           
      StringBuffer sbuf = new StringBuffer();
      long now = System.currentTimeMillis();
            
      for (int i = 0; i < 10000000; i++)
          sbuf.append("*");
          
      System.out.println("Time using StringBuffer: " + (System.currentTimeMillis() - now) + " ms");

      now = System.currentTimeMillis();
      
      StringBuilder sbuild = new StringBuilder();
       
      for (int i = 0; i < 10000000; i++)
         sbuild.append("*");
       
      System.out.println("Time using StringBuilder: " + (System.currentTimeMillis() - now) + " ms");
   }
}
