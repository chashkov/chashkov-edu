package school.lesson4.mutable;

public class BuilderStringCompare {
  
   public static void main(String[] args) {
         
     
      String s = new String();
      
      long now = System.currentTimeMillis();
            
      for (int i = 0; i < 1000000; i++)
          s += "*";
          
      System.out.println("Time using String concat: " + (System.currentTimeMillis() - now) + " ms");

      now = System.currentTimeMillis();
      
      StringBuilder sbuild = new StringBuilder();
       
      for (int i = 0; i < 1000000; i++)
         sbuild.append("*");
       
      System.out.println("Time using StringBuilder: " + (System.currentTimeMillis() - now) + " ms");
   }
}
