package school.lesson4.mutable;

public class ReverseDemo {

	public static void main(String args[]) {
	   
		StringBuffer sb = new StringBuffer("Dot saw I was Tod");

		System.out.println("Before reverse: " + sb);
		sb.reverse();
		
		System.out.println("After reverse: " + sb);
	}
}