package school.lesson4.strings;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class GetBytesDemo {

   public static void main(String[] args)  throws UnsupportedEncodingException{

      String greeting = "������ ���!";
      System.out.println("Old string: " + greeting);
      System.out.println("Old string length: " + greeting.length() );
      
      byte[] byteArray = greeting.getBytes("UTF-8");
      System.out.println("Byte array: " + Arrays.toString(byteArray));
      System.out.println("Byte array length: " + byteArray.length);
            
      String hello = new String(byteArray,"UTF-8"); 
      System.out.println("New string after reconstruction: " + hello );
      System.out.println("New string length: " + hello.length() );
   }
}
