package school.lesson4.strings;

public class CharacterDemo {

	public static void main(String args[]) {
		char c = 'a';
		boolean lowerCase = Character.isLowerCase(c);
		boolean upperCase = Character.isUpperCase(c);
		boolean letterOrDigit = Character.isLetter(c);
		System.out.println("Is c lowercase? " + lowerCase);
		System.out.println("Is c uppercase? " + upperCase);
		System.out.println("Is c a letter? " + letterOrDigit);
	}
}
