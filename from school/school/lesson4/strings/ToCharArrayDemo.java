package school.lesson4.strings;

public class ToCharArrayDemo {

   public static void main(String[] args) {

      String palindrome = "Dot saw I was Tod";
      int len = palindrome.length();
      
      char[] tempCharArray = palindrome.toCharArray();
      char[] charArray = new char[len];
      
      for (int j = 0; j < len; j++) {
         charArray[j] = tempCharArray[len - 1 - j];
      }

      String reversePalindrome = new String(charArray);
      System.out.println(reversePalindrome);
   }
}
