package school.lesson4.strings;

public class MatchDemo {

   public static void main(String[] args) {

      String[] names = { "Girl Reading at a Table", "Gertrude Stein",
            "Guitar and Clarinet on a Mantelpiece", "Woman Ironing",
            "Head of a Woman", "Bust of a Man", "Woman in Green",
            "Mother and Child by a Fountain", "At the Lapin Agile",
            "Mandolin, Fruit Bowl, and Plaster Arm", "Woman in an Armchair",
            "Harlequin", "Seated Harlequin", "Woman in White",
            "Woman Asleep at a Table", "Dying Bull", "Self-Portrait",
            "Man with a Lollipop", "Woman in Profile", "The Actor" };

      for (String name : names) {

         if (name.startsWith("Woman")) {
            System.out.println(name);
         }
      }
   }
}
