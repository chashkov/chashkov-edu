package school.lesson4.regex;

import java.util.regex.Pattern;

public class Time24CheckDemo {

   public static void main(String[] args) {

      final String TIME24_PATTERN = "^([01]?[0-9]|2[0-3]):[0-5][0-9]$";

      String[] time24s = { "23:59", "24:00", "00:00", "12:00", "22:62" };

      for (String time24 : time24s) {
         if (Pattern.matches(TIME24_PATTERN, time24)) {
            System.out.println(time24 + " is a valid time in 24 hour format");
         } else {
            System.out.println(time24 + " is not a valid time in 24 hour format");
         }
      }
   }
}
