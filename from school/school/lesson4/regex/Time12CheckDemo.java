package school.lesson4.regex;

import java.util.regex.Pattern;

public class Time12CheckDemo {

   public static void main(String[] args) {

      final String TIME12_PATTERN = "^(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)$";

      String[] time12s = { "12:00 am", "12:59 pm", "1:00 am", "12:00pm", "22:62pm" };

      for (String time12 : time12s) {
         if (Pattern.matches(TIME12_PATTERN, time12)) {
            System.out.println(time12 + " is a valid time in 12 hour format");
         } else {
            System.out.println(time12 + " is not a valid time in 12 hour format");
         }
      }
   }
}
