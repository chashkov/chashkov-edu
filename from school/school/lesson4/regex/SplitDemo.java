package school.lesson4.regex;

import java.util.regex.Pattern;

public class SplitDemo {

   public static void main(String[] args) {

      String text = " A separator Text separator With separator Many separator Separators";
      String patternString = "separator";

      Pattern pattern = Pattern.compile(patternString);
      String[] chunks = pattern.split(text);

      for (String s : chunks) {
         System.out.println(s);
      }
   }
}
