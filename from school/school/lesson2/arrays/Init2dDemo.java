package school.lesson2.arrays;

public class Init2dDemo {

   public static void main(String[] args) {

      int[][] anArray;
      anArray = new int[2][3];

      anArray[0][0] = 0;
      anArray[0][1] = 1;
      anArray[0][2] = 2;
      anArray[1][0] = 3;
      anArray[1][1] = 4;
      anArray[1][2] = 5;

      for (int i = 0; i < 2; i++) {
         for (int j = 0; j < 3; j++) {

            System.out.print(anArray[i][j] + " ");
         }
         System.out.println();
      }
   }
}
