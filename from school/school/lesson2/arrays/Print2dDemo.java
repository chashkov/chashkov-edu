package school.lesson2.arrays;

import java.util.Arrays;

public class Print2dDemo {

   public static void main(String[] args) {

      int[][] anArray = { { 1 }, { 1, 2 }, { 1, 2, 3 }, { 1, 2, 3, 4 }, { 1, 2, 3, 4 ,5} };

      System.out.println(anArray.getClass().getName() + "@" + Integer.toHexString(anArray.hashCode()));
      System.out.println(anArray);
      System.out.println(Arrays.toString(anArray));
      System.out.println(Arrays.deepToString(anArray));
   }
}