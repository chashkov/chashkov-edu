package school.lesson2.wrappers;

public class InterestingDemo {

   public static void main(String[] args) {

      Integer i = 127;
      Integer j = 127;
      System.out.println("i==j is " + (i == j));
      //true
      
      Integer b = new Integer(127);
      Integer a = 127;
      System.out.println("a==b is " + (a == b));
      //false
      
      Integer k = 128;
      Integer l = 128;
      System.out.println("k==l is " + (k == l));
      //false
   }
}