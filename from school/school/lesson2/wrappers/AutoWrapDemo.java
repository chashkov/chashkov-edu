package school.lesson2.wrappers;

public class AutoWrapDemo {


	public static void main(String[] args) {
		
	      boolean boo = false;
	      Boolean wboo = boo;  
	      System.out.println("boolean variable's value: " + wboo); 
	      
	      byte b = 2;
	      Byte wbyte = b;
	      System.out.println("byte variable's value: " + wbyte); 
	      
	      short s = 4;
	      Short wshort = s;
	      System.out.println("short variable's value: " + wshort); 
	      
	      int i = 16;
	      Integer wint = i;
	      System.out.println("int variable's value: " + wint); 
	      
	      long l = 123;
	      Long wlong = l;
	      System.out.println("long variable's value: " + wlong); 
	      
	      float f = 12.34f;
	      Float wfloat = f;
	      System.out.println("float variable's value: " + wfloat); 
	      
	      double d = 12.56d;
	      Double wdouble = d;
	      System.out.println("double variable's value: " + wdouble); 
     }
}