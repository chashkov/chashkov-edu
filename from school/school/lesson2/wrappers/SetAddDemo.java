package school.lesson2.wrappers;

import java.util.HashSet;
//import java.util.TreeSet;
import java.util.Set;

public class SetAddDemo {

   public static void main(String[] args) {
      String[] arguments = { "apple", "orange", "kiwi", "apple", "apple",
            "pear", "pear", "apple", "orange" };

      Set fruits = new HashSet();
      //Set<String> words = new LinkedHashSet<String>();
      //Set<String> words = new TreeSet<String>();
      
      for (String f : arguments) {
         boolean wasAdded = fruits.add(f);
         if (wasAdded == true)
            System.out.println(f + " was added succesfully, set size: " + fruits.size());
         else
            System.out.println(f + " was not added");
      }

      System.out.println("\nSet contents: " + fruits);
   }
}
