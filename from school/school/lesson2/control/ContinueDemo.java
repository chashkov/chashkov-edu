package school.lesson2.control;

public class ContinueDemo {

   public static void main(String[] args) {
      
      String[] people = { "Tom", "Alice", "Bob", "John", "Harry", "Don", "Tony", "Carol" };
      
      for (int i = 0; i < people.length; i++) {
         if (people[i].equals("Don")) {
            System.out.println("Found  criminal Don");
            continue;
         }
         if (people[i].equals("John")) {
            System.out.println("Found criminal John");
            continue;
         }
         System.out.println("Next person is: " + people[i]); 
      }
   }
}
