package school.lesson2.primitives;


public class ImprecisionDemo {

   public static void main(String[] args) {

      System.out.println(0.125);
      System.out.println(0.125 + 0.125);
      System.out.println(0.125 + 0.125 + 0.125);

   }
}
