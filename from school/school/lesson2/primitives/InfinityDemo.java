package school.lesson2.primitives;

public class InfinityDemo {

   public static void main(String[] args) {

      double a = 10.0;
      double b = 0.0;
      double c = -1.0;

      System.out.println("10.0 divided by 0.0 is: " + a / b);
      System.out.println("square root of -1 is: " + Math.sqrt(c));

   }
}
